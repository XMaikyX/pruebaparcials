﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTienda
{
    class Reserva
    {
        private List<DispositivosTecnologicos> listaDeReserva;

        public List<DispositivosTecnologicos> ListaDeReserva
        {
            get { return listaDeReserva; }
            set { listaDeReserva = value; }
        }
        public void AgregarDispositivo( DispositivosTecnologicos nuevoDispositivo)
        {
            this.ListaDeReserva.Add(nuevoDispositivo);
        }
        public void EliminarDispositivo(DispositivosTecnologicos eliminardispositivo)
        {
            this.ListaDeReserva.Remove(eliminardispositivo);
        }
        private List<Cliente> clientes;

        public List<Cliente> Clientes
        {
            get { return clientes; }
            set { clientes = value; }
        }
        public void AgregarCliente(Cliente Nuevocliente)
        {
            this.Clientes.Add(Nuevocliente);
        }
        public void RetirarCliente(Cliente Retiradocliente)
        {
            this.Clientes.Remove(Retiradocliente);
        }

    }
}
